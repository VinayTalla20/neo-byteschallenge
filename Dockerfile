FROM ubuntu
ADD *.sh my_first_script.sh
RUN chmod +x my_first_script.sh
ENTRYPOINT ["/bin/bash", "my_first_script.sh"]
